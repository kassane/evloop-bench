import vibe.core.core;
import std.stdio;
import std.concurrency;
import core.atomic;
import core.time;

void main()
{
    enum NUM_TASKS = 100_0000; // Number of tasks to run
    enum NUM_ITERATIONS = 100; // Number of iterations to benchmark

    writeln("Starting event-loop benchmark...");

    auto t1 = spawn({

        auto startTime = MonoTime.currTime();

        runTask({
            foreach (_; 0 .. NUM_ITERATIONS)
                foreach (__; 0 .. NUM_TASKS)
                {
                    // This is a placeholder for the task that you want to benchmark.
                    // You can place your actual task code here.
                }
        });

        auto endTime = MonoTime.currTime();
        auto totalTime = endTime - startTime;
        writeln("Benchmark completed in ", totalTime);
        exitEventLoop(true);
    });
    t1.send(true);
    runApplication();
}
