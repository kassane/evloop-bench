
#[tokio::main]
async fn main() {
    let num_tasks = 1000000; // Number of tasks to run
    let num_iterations = 100; // Number of iterations to benchmark

    println!("Starting event-loop benchmark...");

    let start_time = std::time::Instant::now();

    for _ in 0..num_iterations {
        let mut tasks = Vec::new();

        for _ in 0..num_tasks {
            let task = tokio::spawn(async {
                // This is a placeholder for the task that you want to benchmark.
            });
            tasks.push(task);
        }

        for task in tasks {
            task.await.unwrap();
        }
    }

    let end_time = std::time::Instant::now();
    let total_time = end_time.duration_since(start_time);

    println!("Benchmark completed in {:?} ms.", total_time);
}
