#include <iostream>
#include <asio.hpp>
#include <chrono>

int main() {
    asio::io_context io;

    constexpr int numTasks = 1000000; // Number of tasks to run
    constexpr int numIterations = 100; // Number of iterations to benchmark

    std::cout << "Starting event-loop benchmark..." << std::endl;

    auto startTime = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < numIterations; ++i) {
        for (int j = 0; j < numTasks; ++j) {
            asio::post(io, []() {
                // This is a placeholder for the task that you want to benchmark.
                // You can place your actual task code here.
            });
        }
        io.run();
        io.restart(); // Reset the io_context for the next iteration
    }

    auto endTime = std::chrono::high_resolution_clock::now();
    auto totalTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();

    std::cout << "Benchmark completed in " << totalTime << " ms." << std::endl;

    return 0;
}

