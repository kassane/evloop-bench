# Event-Loop Benchmark: D, Rust, and C++

This repository contains a set of benchmarks comparing the performance of event-driven event loops in three different programming languages: D, Rust, and C++. These benchmarks are designed to evaluate the efficiency and performance of event-driven concurrency in these languages.

## Table of Contents

- [Motivation](#motivation)
- [Prerequisites](#prerequisites)
- [Usage](#usage)
- [Benchmark Results](#benchmark-results)
- [Contributing](#contributing)
- [License](#license)

## Motivation

Modern software often relies on event-driven architectures and event loops for efficient handling of I/O-bound and concurrent tasks. Understanding the performance characteristics of event loops in different programming languages can be crucial when choosing the right technology stack for your project.

This benchmark aims to compare the event-driven performance of D (using Vibe.d), Rust (using Tokio), and C++ (using Asio) for a set of typical use cases.

## Prerequisites

To run and evaluate these benchmarks, you will need to have the following prerequisites installed:

- D compiler and Vibe.d (for D)
- Rust compiler and Tokio (for Rust)
- C++ compiler with Asio (for C++)

## Usage

1. **Clone the Repository:**

```bash
   git clone https://gitlab.com/kassane/evloop-bench.git
   cd evloop-bench
```
**C++ build**
```bash
cd cpp_asio
cmake -B build -DCMAKE_BUILD_TYPE=Release
cmake --build build --config release --target run 
```

**D build**
```bash
cd d_vibeloop
dub --build=release
```

**Rust build**
```bash
cd tokio_evloop
cargo run --release 
```

## benchmark-result

tool used: https://github.com/andrewrk/poop/releases

```bash
poop -d 100 'cpp_asio/build/asio_evloop' 'tokio_evloop/target/release/tokio_evloop' 'd_vibeloop/build/d_vibeloop'
Benchmark 1 (3 runs): cpp_asio/build/asio_evloop
  measurement          mean ± σ            min … max           outliers         delta
  wall_time          3.32s  ±  100ms    3.21s  … 3.41s           0 ( 0%)        0%
  peak_rss           51.5MB ± 37.8KB    51.5MB … 51.5MB          0 ( 0%)        0%
  cpu_cycles         14.8G  ±  348M     14.4G  … 15.1G           0 ( 0%)        0%
  instructions       47.3G  ± 26.8K     47.3G  … 47.3G           0 ( 0%)        0%
  cache_references    348M  ±  177K      348M  …  348M           0 ( 0%)        0%
  cache_misses       39.0M  ±  896K     38.1M  … 39.8M           0 ( 0%)        0%
  branch_misses      68.5K  ± 9.13K     63.0K  … 79.0K           0 ( 0%)        0%
Benchmark 2 (3 runs): tokio_evloop/target/release/tokio_evloop
  measurement          mean ± σ            min … max           outliers         delta
  wall_time          31.6s  ± 66.8ms    31.5s  … 31.6s           0 ( 0%)        💩+850.5% ±  5.8%
  peak_rss            268MB ±  920KB     267MB …  269MB          0 ( 0%)        💩+420.4% ±  2.9%
  cpu_cycles          182G  ±  965M      182G  …  183G           0 ( 0%)        💩+1130.9% ± 11.1%
  instructions        215G  ± 71.9M      215G  …  215G           0 ( 0%)        💩+354.9% ±  0.2%
  cache_references   5.12G  ± 33.0M     5.09G  … 5.16G           0 ( 0%)        💩+1371.2% ± 15.2%
  cache_misses       1.24G  ± 2.73M     1.23G  … 1.24G           0 ( 0%)        💩+3070.5% ± 11.8%
  branch_misses       336M  ± 2.74M      333M  …  339M           0 ( 0%)        💩+490904.0% ± 6411.8%
Benchmark 3 (41 runs): d_vibeloop/build/d_vibeloop
  measurement          mean ± σ            min … max           outliers         delta
  wall_time          2.46ms ±  190us    2.24ms … 3.35ms          1 ( 2%)        ⚡- 99.9% ±  0.8%
  peak_rss           7.35MB ± 71.5KB    7.21MB … 7.47MB         12 (29%)        ⚡- 85.7% ±  0.2%
  cpu_cycles         4.68M  ±  557K     4.48M  … 8.10M           4 (10%)        ⚡-100.0% ±  0.6%
  instructions       9.57M  ± 1.69K     9.57M  … 9.58M           5 (12%)        ⚡-100.0% ±  0.0%
  cache_references    245K  ± 1.74K      243K  …  252K           4 (10%)        ⚡- 99.9% ±  0.0%
  cache_misses       63.8K  ±  864      62.4K  … 66.2K           2 ( 5%)        ⚡- 99.8% ±  0.6%
  branch_misses      46.3K  ±  133      46.1K  … 46.6K           0 ( 0%)        ⚡- 32.4% ±  3.5%
```

## Contributing

Feel free to contribute to this benchmark project by adding more test cases, optimizing the existing code, or sharing your own performance results in different scenarios and use cases. If you have any suggestions or improvements, please submit a pull request.

## License

This benchmark project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details.